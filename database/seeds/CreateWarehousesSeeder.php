<?php

use Illuminate\Database\Seeder;
use App\WareHouse;

class CreateWarehousesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['name' => 'Warehouse 1', 'address' => 'lower parel, mumbai, 400013'],
            ['name' => 'Warehouse 2', 'address' => 'mazgaon, mumbai, 400010'],
            ['name' => 'Warehouse 3', 'address' => 'Dadar, mumbai, 400016'],
            ['name' => 'Warehouse 4', 'address' => 'worli, mumbai, 400012']
        ];

        foreach ($data as $d) {
            WareHouse::create($d);
        }
    }
}
