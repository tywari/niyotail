<?php

use Illuminate\Database\Seeder;
use App\Product;

class CreateProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['name' => 'product 1', 'price' => 2000, 'warehouse_id' => 1, 'in_stock' => 10],
            ['name' => 'product 2', 'price' => 2500, 'warehouse_id' => 2, 'in_stock' => 10],
            ['name' => 'product 3', 'price' => 1100, 'warehouse_id' => 3, 'in_stock' => 10],
            ['name' => 'product 4', 'price' => 4300, 'warehouse_id' => 4, 'in_stock' => 10],
            ['name' => 'product 5', 'price' => 1200, 'warehouse_id' => 1, 'in_stock' => 10],
            ['name' => 'product 6', 'price' => 1400, 'warehouse_id' => 2, 'in_stock' => 10],
            ['name' => 'product 7', 'price' => 4200, 'warehouse_id' => 3, 'in_stock' => 10],
            ['name' => 'product 8', 'price' => 1800, 'warehouse_id' => 4, 'in_stock' => 10],
        ];

        foreach ($data as $d) {
            Product::create($d);
        }
    }
}
