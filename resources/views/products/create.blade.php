@extends('layouts.app')


@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Add Purchase Order</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('products.index') }}"> Back</a>
            </div>
        </div>
    </div>


    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


    <form action="{{ route('products.store') }}" method="POST">
    	@csrf


         <div class="row">

             <div class="col-xs-12 col-sm-12 col-md-12">
                 <div class="form-group">
                     <strong>Select WareHouse:</strong><br>
                     {!! Form::select('warehouse_name', $warehouse, array('class' => 'form-control')) !!}
                 </div>
             </div>
             <div class="col-xs-12 col-sm-12 col-md-12">
                 <div class="form-group">
                     <strong>Select Product:</strong> <br>
                     <select name="product_id">
                     @foreach($products as $product)
                             <option value="{{$product->id}}">{{$product->name}}</option>
                         @endforeach
                     </select>

                 </div>
             </div>

             <div class="col-xs-12 col-sm-12 col-md-12">
                 <div class="form-group">
                     <strong>Quantity:</strong>
                     <input type="text" name="quantity" class="form-control" placeholder="quantity">
                 </div>
             </div>

             <div class="col-xs-12 col-sm-12 col-md-12">
                 <div class="form-group">
                     <strong>Total Cost:</strong>
                     <input type="text" name="total_cost" class="form-control" placeholder="Total Cost">
                 </div>
             </div>

             <div class="col-xs-12 col-sm-12 col-md-12">
                 <div class="form-group">
                     <strong>Expiry Date:</strong>
                     <input type="text" name="expiry_date" class="form-control" placeholder="Expiry Date">
                 </div>
             </div>


		    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
		            <button type="submit" class="btn btn-primary">Submit</button>
		    </div>
		</div>


    </form>

<p class="text-center text-primary"><small>Project for niyotail.com</small></p>
@endsection
