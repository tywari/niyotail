<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WareHouse extends Model
{
    protected $table = 'warehouses';
    protected $fillable = [
        'name', 'address'
    ];
}
