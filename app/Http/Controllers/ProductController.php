<?php


namespace App\Http\Controllers;


use App\Product;
use App\PurchaseOrder;
use App\WareHouse;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;


class ProductController extends Controller
{
    /**
     * @dev Display a listing of the resource.
     *
     */
    function __construct()
    {
         $this->middleware('permission:product-list|product-create|product-edit|product-delete', ['only' => ['index','show']]);
         $this->middleware('permission:product-create', ['only' => ['create','store']]);
         $this->middleware('permission:product-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:product-delete', ['only' => ['destroy']]);
    }
    /**
     * @dev Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $products = Product::latest()->paginate(5);
        return view('products.index',compact('products'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }


    /**
     * @dev Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $warehouse = WareHouse::pluck('name','name')->all();
        $products = Product::all();
        return view('products.create',compact('warehouse','products'));
    }


    /**
     * @dev Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'quantity' => 'required',
            'warehouse_name' => 'required',
            'product_id' => 'required',
            'total_cost' => 'required',
            'expiry_date' => 'required',
        ]);

        PurchaseOrder::create($request->all());

        $products = Product::lockForUpdate()->find($request['product_id']);
        $products->in_stock += $request['quantity'];
        $products->save();

        return redirect()->route('products.index')
                        ->with('success','Product created successfully.');
    }


    /**
     * @dev Display the specified resource.
     * @param Product $product
     * @return Response
     */
    public function show(Product $product)
    {
        return view('products.show',compact('product'));
    }


    /**
     * @dev Show the form for editing the specified resource.
     * @param Product $product
     * @return Response
     */
    public function edit(Product $product)
    {
        return view('products.edit',compact('product'));
    }


    /**
     * @dev Update the specified resource in storage.
     * @param Request $request
     * @param Product $product
     * @return Response
     */
    public function update(Request $request, Product $product)
    {
         request()->validate([
             'name' => 'required',
             'in_stock' => 'required',
             'price' => 'required'
        ]);
        $product->update($request->all());

        return redirect()->route('products.index')
                        ->with('success','Product updated successfully');
    }


    /**
     * @dev Remove the specified resource from storage.
     * @param Product $product
     * @return Response
     * @throws Exception
     */
    public function destroy(Product $product)
    {
        $product->delete();

        return redirect()->route('products.index')
                        ->with('success','Product deleted successfully');
    }
}
